const request = require('supertest');
const app = require("./index");

describe('API register tests', () => {
    it('Register successful', async() => {
        const response = await request(app).post("/register").send({
            user: "admin",
            pass: "admin"
        });
        expect(response.body).toEqual({status: 1, user: "admin"});
    });

    it('Register failed', async() => {
        const response = await request(app).post("/register").send({
            user: "admin0",
            pass: "admin"
        });
        expect(response.body).toEqual({status: 0});
    });
});

describe('API login tests', () => {

    it('Login failed', async() => {
        const response = await request(app).post("/login").send({
            user: "admin",
            pass: "admin0"
        });
        expect(response.body).toEqual({status: 0});
    });

    it('Login successful', async() => {
        const response = await request(app).post("/login").send({
            user: "admin",
            pass: "admin"
        });
        expect(response.body).toEqual({status: 1, token: expect.any(Number)});
    });

});

describe('API crud test', () => {
    it('Create successful', async() => {
        const response = await request(app).post("/create").send({
            token: 1,
            title: "create todo test",
            desc: "this is a test create from my todo app"
        });
        expect(response.body).toEqual({status: 1});
    });

    it('Delete successful', async() => {
        const response = await request(app).post("/delete").send({
            id : 1,
            token: 1
        });
        expect(response.body).toEqual({status: 1});
    });

    it('Todos gets', async() => {
        const response = await request(app).post("/todos").send({
            id : 1
        });
        expect(response.body).toEqual({status: expect.any(Number), data: expect.any(Array)});
    });

});